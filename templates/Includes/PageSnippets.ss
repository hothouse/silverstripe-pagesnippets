<div class="snippets row">
	<% loop $PageSnippetList %>
	<article class="col-md-4">
		<div class="pane">
			<% if SnippetImage %>
			<img class="img-responsive" src="$SnippetImage.URL" alt="$SnippetImage.Title" />
			<% end_if %>

			<h2>$Title</h2>

			<% if SubTitle %>
			<h3>$SubTitle</h3>
			<% end_if %>

			$Content

			<a href="$PageLink"<% if $LinkNewTab %> target="_blank"<% end_if %>>$LinkTitle</a>
		</div>
	</article>
	<% end_loop %>
</div>